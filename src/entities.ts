export namespace de.tammenit.securitycalc {
  export enum DayShiftType {
    weekday = 1,
    weekend = 2,
    bankholiday = 3,
  }

  export interface ICalcSheet {
    createdAt?: Date;
    createdBy?: string;
    modifiedAt?: Date;
    modifiedBy?: string;
    ID?: string;
    name: string;
    descr: string;
    startTime: Date;
    endTime: Date;
    resources: IResources[];
    dayShiftWeekday?: IDayShifts;
    dayShiftWeekday_ID?: string;
    dayShiftWeekday_Text: string;
    nightShiftWeekday?: IDayShifts;
    nightShiftWeekday_ID?: string;
    nightShiftWeekday_Text: string;
    dayShiftWeekend?: IDayShifts;
    dayShiftWeekend_ID?: string;
    dayShiftWeekend_Text: string;
    nightShiftWeekend?: IDayShifts;
    nightShiftWeekend_ID?: string;
    nightShiftWeekend_Text: string;
    dayShiftBankholiday?: IDayShifts;
    dayShiftBankholiday_ID?: string;
    dayShiftBankholiday_Text: string;
    nightShiftBankholiday?: IDayShifts;
    nightShiftBankholiday_ID?: string;
    nightShiftBankholiday_Text: string;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface IDayShiftTypeValues {
    value: number;
    name: string;
  }

  export interface IDayShifts {
    createdAt?: Date;
    createdBy?: string;
    modifiedAt?: Date;
    modifiedBy?: string;
    ID?: string;
    shortTxt: string;
    descr: string;
    startTime: Date;
    endTime: Date;
    dayShiftType_ID: number;
    dayShiftType_Text: string;
    dayShiftType?: IDayShiftTypeValues;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface ISecurityEmployeeTypes {
    createdAt?: Date;
    createdBy?: string;
    modifiedAt?: Date;
    modifiedBy?: string;
    validFrom: Date;
    validTo: Date;
    ID?: string;
    shortTxt: string;
    descr: string;
    dayRate: number;
    nightRate: number;
    bankholidayRate: number;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface IResources {
    up_?: ICalcSheet;
    up__ID?: string;
    createdAt?: Date;
    createdBy?: string;
    modifiedAt?: Date;
    modifiedBy?: string;
    ID: string;
    name: string;
    amount: number;
    employeeType?: ISecurityEmployeeTypes;
    employeeType_ID?: string;
    employeeType_Text: string;
    startTime: Date;
    endTime: Date;
    weekdayStartTime: Date;
    weekdayEndTime: Date;
    weekendStartTime: Date;
    weekendEndTime: Date;
    bankholidayStartTime: Date;
    bankholidayEndTime: Date;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface ITexts {
    locale: string;
    up_?: ICalcSheet;
    up__ID?: string;
    ID: string;
    name: string;
  }

  export interface ITexts {
    locale: string;
    ID?: string;
    name: string;
    descr: string;
  }

  export interface ITexts {
    locale: string;
    ID?: string;
    shortTxt: string;
    descr: string;
  }

  export interface ITexts {
    locale: string;
    ID?: string;
    shortTxt: string;
    descr: string;
  }

  export enum Entity {
    CalcSheet = "de.tammenit.securitycalc.CalcSheet",
    DayShiftTypeValues = "de.tammenit.securitycalc.DayShiftTypeValues",
    DayShifts = "de.tammenit.securitycalc.DayShifts",
    SecurityEmployeeTypes = "de.tammenit.securitycalc.SecurityEmployeeTypes",
    Resources = "de.tammenit.securitycalc.CalcSheet.resources",
    Texts = "de.tammenit.securitycalc.SecurityEmployeeTypes.texts",
  }

  export enum SanitizedEntity {
    CalcSheet = "CalcSheet",
    DayShiftTypeValues = "DayShiftTypeValues",
    DayShifts = "DayShifts",
    SecurityEmployeeTypes = "SecurityEmployeeTypes",
    Resources = "Resources",
    Texts = "Texts",
  }
}

export namespace sap.common {
  export interface ILanguages {
    name: string;
    descr: string;
    code: string;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface ICountries {
    name: string;
    descr: string;
    code: string;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface ICurrencies {
    name: string;
    descr: string;
    code: string;
    symbol: string;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface ITexts {
    locale: string;
    name: string;
    descr: string;
    code: string;
  }

  export interface ITexts {
    locale: string;
    name: string;
    descr: string;
    code: string;
  }

  export interface ITexts {
    locale: string;
    name: string;
    descr: string;
    code: string;
  }

  export enum Entity {
    Languages = "sap.common.Languages",
    Countries = "sap.common.Countries",
    Currencies = "sap.common.Currencies",
    Texts = "sap.common.Currencies.texts",
  }

  export enum SanitizedEntity {
    Languages = "Languages",
    Countries = "Countries",
    Currencies = "Currencies",
    Texts = "Texts",
  }
}

export namespace de.tammenit.securitycalc.srv.SeccalcService {
  export interface ICalcResult {
    val: number;
    str: string;
  }

  export interface ICalcSheet {
    createdAt?: Date;
    modifiedAt?: Date;
    ID?: string;
    name: string;
    descr: string;
    startTime: Date;
    endTime: Date;
    resources: IResources[];
    dayShiftWeekday?: IDayShifts;
    dayShiftWeekday_ID?: string;
    dayShiftWeekday_Text: string;
    nightShiftWeekday?: IDayShifts;
    nightShiftWeekday_ID?: string;
    nightShiftWeekday_Text: string;
    dayShiftWeekend?: IDayShifts;
    dayShiftWeekend_ID?: string;
    dayShiftWeekend_Text: string;
    nightShiftWeekend?: IDayShifts;
    nightShiftWeekend_ID?: string;
    nightShiftWeekend_Text: string;
    dayShiftBankholiday?: IDayShifts;
    dayShiftBankholiday_ID?: string;
    dayShiftBankholiday_Text: string;
    nightShiftBankholiday?: IDayShifts;
    nightShiftBankholiday_ID?: string;
    nightShiftBankholiday_Text: string;
    texts: ITexts[];
    localized?: ITexts;
  }

  export namespace ICalcSheet.actions {
    export enum FuncStartCalc {
      name = "startCalc",
    }

    export type FuncStartCalcReturn =
      de.tammenit.securitycalc.srv.SeccalcService.ICalcResult;
  }

  export interface ISecurityEmployeeTypes {
    createdAt?: Date;
    modifiedAt?: Date;
    validFrom: Date;
    validTo: Date;
    ID?: string;
    shortTxt: string;
    descr: string;
    dayRate: number;
    nightRate: number;
    bankholidayRate: number;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface IDayShifts {
    createdAt?: Date;
    createdBy?: string;
    modifiedAt?: Date;
    modifiedBy?: string;
    ID?: string;
    shortTxt: string;
    descr: string;
    startTime: Date;
    endTime: Date;
    dayShiftType_ID: number;
    dayShiftType_Text: string;
    dayShiftType?: IDayShiftTypeValues;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface IDayShiftTypeValues {
    value: number;
    name: string;
  }

  export interface IResources {
    up_?: ICalcSheet;
    up__ID?: string;
    createdAt?: Date;
    createdBy?: string;
    modifiedAt?: Date;
    modifiedBy?: string;
    ID: string;
    name: string;
    amount: number;
    employeeType?: ISecurityEmployeeTypes;
    employeeType_ID?: string;
    employeeType_Text: string;
    startTime: Date;
    endTime: Date;
    weekdayStartTime: Date;
    weekdayEndTime: Date;
    weekendStartTime: Date;
    weekendEndTime: Date;
    bankholidayStartTime: Date;
    bankholidayEndTime: Date;
    texts: ITexts[];
    localized?: ITexts;
  }

  export interface ITexts {
    locale: string;
    ID?: string;
    name: string;
    descr: string;
  }

  export interface ITexts {
    locale: string;
    ID?: string;
    shortTxt: string;
    descr: string;
  }

  export interface ITexts {
    locale: string;
    ID?: string;
    shortTxt: string;
    descr: string;
  }

  export interface ITexts {
    locale: string;
    up_?: ICalcSheet;
    up__ID?: string;
    ID: string;
    name: string;
  }

  export enum Entity {
    CalcResult = "de.tammenit.securitycalc.srv.SeccalcService.CalcResult",
    CalcSheet = "de.tammenit.securitycalc.srv.SeccalcService.CalcSheet",
    SecurityEmployeeTypes = "de.tammenit.securitycalc.srv.SeccalcService.SecurityEmployeeTypes",
    DayShifts = "de.tammenit.securitycalc.srv.SeccalcService.DayShifts",
    DayShiftTypeValues = "de.tammenit.securitycalc.srv.SeccalcService.DayShiftTypeValues",
    Resources = "de.tammenit.securitycalc.srv.SeccalcService.CalcSheet.resources",
    Texts = "de.tammenit.securitycalc.srv.SeccalcService.CalcSheet.resources.texts",
  }

  export enum SanitizedEntity {
    CalcResult = "CalcResult",
    CalcSheet = "CalcSheet",
    SecurityEmployeeTypes = "SecurityEmployeeTypes",
    DayShifts = "DayShifts",
    DayShiftTypeValues = "DayShiftTypeValues",
    Resources = "Resources",
    Texts = "Texts",
  }
}

export type User = string;

export enum Entity {}

export enum SanitizedEntity {}
