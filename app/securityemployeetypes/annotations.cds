using de.tammenit.securitycalc.srv.SeccalcService as service from '../../srv/seccalc-service';

annotate service.SecurityEmployeeTypes with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Value : shortTxt,
        },
        {
            $Type : 'UI.DataField',
            Value : dayRate,
        },
        {
            $Type : 'UI.DataField',
            Value : nightRate,
        },
        {
            $Type : 'UI.DataField',
            Value : bankholidayRate,
        },
        {
            $Type : 'UI.DataField',
            Value : validFrom,
        },
        {
            $Type : 'UI.DataField',
            Value : validTo,
        },
    ],
	UI: {
		HeaderInfo: {
			TypeName: 'Security employee type',
			TypeNamePlural: 'Security employee types',
			Title : {
          $Type : 'UI.DataField',
          Value : shortTxt
      },
			Description : {
				$Type: 'UI.DataField',
				Value: descr
			},
		},
    SelectionFields: [shortTxt, dayRate, nightRate, bankholidayRate, validFrom],
    Facets: [
      {$Type: 'UI.ReferenceFacet', Label: '{i18n>FieldGroup.SecurityEmployeeType}', Target: '@UI.FieldGroup#Main'}
    ],
    FieldGroup#Main: {
      Data: [
        {Value: dayRate},
        {Value: nightRate},
        {Value: bankholidayRate}
      ]
    }
	},    
) {
  ID @(UI.Hidden)
};


