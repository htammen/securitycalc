sap.ui.define(["sap/fe/core/AppComponent"], function(AppComponent) {
    'use strict';

    return AppComponent.extend("de.tammenit.seccalc.ui.securityemployeetypes.securityemployeetypes.Component", {
        metadata: {
            manifest: "json"
        }
    });
});
