sap.ui.define(["sap/fe/core/AppComponent"], function(AppComponent) {
    'use strict';

    return AppComponent.extend("de.tammenit.sec.ui.calcsheet.calcsheetfe.Component", {
        metadata: {
            manifest: "json"
        }
    });
});
