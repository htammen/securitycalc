sap.ui.define([], function() {
  "use strict";
  //Search-Term: #SideContent
  return {
     toggleSideContent: function(oBindingContext) {
        this.showSideContent("FacetShifts");
     },
     toggleSideContentItem1: function(oContextInfo) {
        this.showSideContent("childEntities1Section");
     }
  };
});
