using de.tammenit.securitycalc.srv.SeccalcService as service from '../../srv/seccalc-service';

annotate service.CalcSheet with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Value : name,
        },
        {
            $Type : 'UI.DataField',
            Label : 'descr',
            Value : descr,
        },
        {
            $Type : 'UI.DataField',
            Label : 'startTime',
            Value : startTime,
        },
        {
            $Type : 'UI.DataField',
            Label : 'endTime',
            Value : endTime,
        },
    ],
    UI.Identification   : [
        {
            $Type                       : 'UI.DataFieldForAction',  //Action in the RootEntities of the object page next to the edit button
            Action                      : 'de.tammenit.securitycalc.srv.SeccalcService.startCalc',
            Label                       : '{i18n>startCalc}',
            Criticality                 : 0,         //Only 0,1,3 supported
            CriticalityRepresentation   : #WithIcon,                //Has no effect
        },
    ],
);
annotate service.CalcSheet with @(
    UI.FieldGroup #GeneratedGroupGeneral : {
        $Type : 'UI.FieldGroupType',
        Data : [
            {
                $Type : 'UI.DataField',
                Value : name,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>Calcsheetdescription}',
                Value : descr,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>Calcsheetstarttime}',
                Value : startTime,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>Calcsheetendtime}',
                Value : endTime,
            },
            {
              $Type : 'UI.DataFieldWithUrl',
              Url :   'https://platform.preview.appgyver.com/307143/first-project/page.Page1',
              Value : 'AppGyver',
              Label : 'Company'
            },
            {
              $Type : 'UI.DataFieldForIntentBasedNavigation',
              Label : 'Berechnungsergebnisse',
              SemanticObject : 'calcsheet',
              Action : 'app',
              RequiresContext: true,
              ![@UI.Importance]: #High 
            } 
        ],
    },
    UI.FieldGroup #GeneratedGroupShifts : {
        $Type : 'UI.FieldGroupType',
        Data : [
            {
                $Type : 'UI.DataField',
                Label : 'dayShiftWeekday',
                Value : dayShiftWeekday_ID,
            },
            {
                $Type : 'UI.DataField',
                Label : 'nightShiftWeekday',
                Value : nightShiftWeekday_ID,
            },
            {
                $Type : 'UI.DataField',
                Label : 'dayShiftWeekend',
                Value : dayShiftWeekend_ID,
            },
            {
                $Type : 'UI.DataField',
                Label : 'nightShiftWeekend',
                Value : nightShiftWeekend_ID,
            },
            {
                $Type : 'UI.DataField',
                Label : 'dayShiftBankholiday',
                Value : dayShiftBankholiday_ID,
            },
            {
                $Type : 'UI.DataField',
                Label : 'nightShiftBankholiday',
                Value : nightShiftBankholiday_ID,
            },
        ],
    },
    UI.Facets : [
        {
            $Type : 'UI.ReferenceFacet',
            ID : 'GeneratedFacetGeneral',
            Label : 'General Information',
            Target : '@UI.FieldGroup#GeneratedGroupGeneral',
        },
        {
            $Type : 'UI.ReferenceFacet',
            ID : 'FacetShifts',
            Label : 'Shift data',
            Target : '@UI.FieldGroup#GeneratedGroupShifts',
        },
        {
            $Type : 'UI.ReferenceFacet',
            ID : 'ResourceFacet',
            Label : '{i18n>Calcsheetresources}',
            Target : 'resources/@UI.LineItem',
        },
    ]
);

annotate service.CalcSheet with {
  dayShiftWeekday @(
    ValueList.entity : 'DayShifts',
    Label: 'Schichtdaten',
    FieldControl: #Mandatory,
    Common.Text: dayShiftWeekday_Text,
    Common.TextArrangement: #TextOnly,
    Common.ValueList: {
      $Type                 : 'Common.ValueListType',
      Label                 : '{i18n>shiftdata}',
      CollectionPath        : 'DayShifts',
      Parameters            : [
        {
          $Type             : 'Common.ValueListParameterInOut',
          LocalDataProperty : 'dayShiftWeekday_ID',
          ValueListProperty : 'ID'
        },
        {
          $Type : 'Common.ValueListParameterOut',
          ValueListProperty : 'shortTxt',
          LocalDataProperty : 'dayShiftWeekday_Text'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'descr'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'startTime'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'endTime'
        }
      ]
    }
  );
  nightShiftWeekday @(
    ValueList.entity : 'DayShifts',
    Label: 'Schichtdaten',
    FieldControl: #Mandatory,
    Common.Text: nightShiftWeekday_Text,
    Common.TextArrangement: #TextOnly,
    Common.ValueList: {
      $Type                 : 'Common.ValueListType',
      Label                 : '{i18n>shiftdata}',
      CollectionPath        : 'DayShifts',
      Parameters            : [
        {
          $Type             : 'Common.ValueListParameterInOut',
          LocalDataProperty : 'nightShiftWeekday_ID',
          ValueListProperty : 'ID'
        },
        {
          $Type : 'Common.ValueListParameterOut',
          ValueListProperty : 'shortTxt',
          LocalDataProperty : 'nightShiftWeekday_Text'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'descr'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'startTime'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'endTime'
        }
      ]
    }
  );
  dayShiftWeekend @(
    ValueList.entity : 'DayShifts',
    Label: 'Schichtdaten',
    FieldControl: #Mandatory,
    Common.Text: dayShiftWeekend_Text,
    Common.TextArrangement: #TextOnly,
    Common.ValueList: {
      $Type                 : 'Common.ValueListType',
      Label                 : '{i18n>shiftdata}',
      CollectionPath        : 'DayShifts',
      Parameters            : [
        {
          $Type             : 'Common.ValueListParameterInOut',
          LocalDataProperty : 'dayShiftWeekend_ID',
          ValueListProperty : 'ID'
        },
        {
          $Type : 'Common.ValueListParameterOut',
          ValueListProperty : 'shortTxt',
          LocalDataProperty : 'dayShiftWeekend_Text'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'descr'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'startTime'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'endTime'
        }
      ]
    }
  );
  nightShiftWeekend @(
    ValueList.entity : 'DayShifts',
    Label: 'Schichtdaten',
    FieldControl: #Mandatory,
    Common.Text: nightShiftWeekend_Text,
    Common.TextArrangement: #TextOnly,
    Common.ValueList: {
      $Type                 : 'Common.ValueListType',
      Label                 : '{i18n>shiftdata}',
      CollectionPath        : 'DayShifts',
      Parameters            : [
        {
          $Type             : 'Common.ValueListParameterInOut',
          LocalDataProperty : 'nightShiftWeekend_ID',
          ValueListProperty : 'ID'
        },
        {
          $Type : 'Common.ValueListParameterOut',
          ValueListProperty : 'shortTxt',
          LocalDataProperty : 'nightShiftWeekend_Text'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'descr'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'startTime'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'endTime'
        }
      ]
    }
  );
  dayShiftBankholiday @(
    ValueList.entity : 'DayShifts',
    Label: 'Schichtdaten',
    FieldControl: #Mandatory,
    Common.Text: dayShiftBankholiday_Text,
    Common.TextArrangement: #TextOnly,
    Common.ValueList: {
      $Type                 : 'Common.ValueListType',
      Label                 : '{i18n>shiftdata}',
      CollectionPath        : 'DayShifts',
      Parameters            : [
        {
          $Type             : 'Common.ValueListParameterInOut',
          LocalDataProperty : 'dayShiftBankholiday_ID',
          ValueListProperty : 'ID'
        },
        {
          $Type : 'Common.ValueListParameterOut',
          ValueListProperty : 'shortTxt',
          LocalDataProperty : 'dayShiftBankholiday_Text'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'descr'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'startTime'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'endTime'
        }
      ]
    }
  );
  nightShiftBankholiday @(
    ValueList.entity : 'DayShifts',
    Label: 'Schichtdaten',
    FieldControl: #Mandatory,
    Common.Text: nightShiftBankholiday_Text,
    Common.TextArrangement: #TextOnly,
    Common.ValueList: {
      $Type                 : 'Common.ValueListType',
      Label                 : '{i18n>shiftdata}',
      CollectionPath        : 'DayShifts',
      Parameters            : [
        {
          $Type             : 'Common.ValueListParameterInOut',
          LocalDataProperty : 'nightShiftBankholiday_ID',
          ValueListProperty : 'ID'
        },
        {
          $Type : 'Common.ValueListParameterOut',
          ValueListProperty : 'shortTxt',
          LocalDataProperty : 'nightShiftBankholiday_Text'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'descr'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'startTime'
        }, 
        {
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'endTime'
        }
      ]
    }
  );
  descr @(
    UI.MultiLineText
  )
};

annotate service.CalcSheet.resources with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Value : name,
        },
        {
            $Type : 'UI.DataField',
            Label : 'amount',
            Value : amount,
        },
        {
            $Type : 'UI.DataField',
            Label : 'startTime',
            Value : startTime,
        },
        {
            $Type : 'UI.DataField',
            Label : 'endTime',
            Value : endTime,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Mitarbeitertyp',
            Value : employeeType_ID,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Weekday start time',
            Value : weekdayStartTime,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Weekday end time',
            Value : weekdayEndTime,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Weekend start time',
            Value : weekendStartTime,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Weekend end time',
            Value : weekendEndTime,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Bank holiday start time',
            Value : bankholidayStartTime,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Bank holiday end time',
            Value : bankholidayEndTime,
        },

    ]
) {
  employeeType @(
    Common: {
      Label : 'Mitarbeitertyp',
      FieldControl: #Mandatory,
      Text: employeeType_Text,
      TextArrangement: #TextOnly,
      ValueListWithFixedValues: false,
      ValueList: {
        Label                 : '{i18n>employeetype}',
        $Type                 : 'Common.ValueListType',
        CollectionPath        : 'SecurityEmployeeTypes',
        Parameters            : [
          {
            $Type             : 'Common.ValueListParameterInOut',
            LocalDataProperty : 'employeeType_ID',
            ValueListProperty : 'ID'
          },{
            $Type : 'Common.ValueListParameterOut',
            ValueListProperty : 'shortTxt',
            LocalDataProperty : employeeType_Text
          }
        ]
      }
    }
  );
};

annotate service.CalcSheet {
  resources @(
    UI.FieldGroup #FGGeneral : {
        $Type : 'UI.FieldGroupType',
        Data : [
            {
                $Type : 'UI.DataField',
                Value : name,
            },
            {
                $Type : 'UI.DataField',
                Label : 'descr',
                Value : amount,
            },
            {
                $Type : 'UI.DataField',
                Label : 'startTime',
                Value : startTime,
            },
            {
                $Type : 'UI.DataField',
                Label : 'endTime',
                Value : endTime,
            },
            {
                $Type : 'UI.DataField',
                Label : 'Employee type',
                Value : employeeType_ID,
            },
        ],
    },
    UI.Facets : [
        {
            $Type : 'UI.ReferenceFacet',
            ID : 'GeneratedFacet1',
            Label : 'General Information',
            Target : '@UI.FieldGroup#FGGeneral',
        },
    ]
  );
  employeeType @(ValueList.entity : 'SecurityEmployeeTypes', 
    Common.ValueList: {
      $Type                 : 'Common.ValueListType',
      CollectionPath        : 'SecurityEmployeeTypes',
      Parameters            : [
        {
          $Type             : 'Common.ValueListParameterInOut',
          LocalDataProperty : 'employeeType_ID',
          ValueListProperty : 'ID'
        },{
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'shortTxt'
        },{
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'dayRate'
        },{
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'nightRate'
        },{
          $Type : 'Common.ValueListParameterDisplayOnly',
          ValueListProperty : 'bankholidayRate'
        }
      ]
    }
  );
};

