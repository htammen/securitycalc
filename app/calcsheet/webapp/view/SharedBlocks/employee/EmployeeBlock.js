sap.ui.define(["sap/uxap/BlockBase"], function(BlockBase) {
  "use strict";

  var EmployeeBlock = BlockBase.extend(
    "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.employee.EmployeeBlock",
    {
      metadata: {
        views: {
          Collapsed: {
            viewName:
              "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.employee.EmployeeBlock",
            type: "XML"
          },
          Expanded: {
            viewName:
              "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.employee.EmployeeBlock",
            type: "XML"
          }
        }
      }
    }
  );

  return EmployeeBlock;
});
