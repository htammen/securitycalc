sap.ui.define(["sap/uxap/BlockBase"], function(BlockBase) {
  "use strict";

  var GeneralBlock = BlockBase.extend(
    "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.general.GeneralBlock",
    {
      metadata: {
        views: {
          Collapsed: {
            viewName:
              "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.general.GeneralBlock",
            type: "XML"
          },
          Expanded: {
            viewName:
              "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.general.GeneralBlock",
            type: "XML"
          }
        }
      }
    }
  );

  return GeneralBlock;
});
