sap.ui.define(["sap/uxap/BlockBase"], function(BlockBase) {
  "use strict";

  var ShiftsBlock = BlockBase.extend(
    "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.shifts.ShiftsBlock",
    {
      metadata: {
        views: {
          Collapsed: {
            viewName:
              "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.shifts.ShiftsBlock",
            type: "XML"
          },
          Expanded: {
            viewName:
              "de.tammenit.sec.ui.calcsheet.view.SharedBlocks.shifts.ShiftsBlock",
            type: "XML"
          }
        }
      }
    }
  );

  return ShiftsBlock;
});
