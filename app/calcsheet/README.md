## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Fri Dec 10 2021 16:46:12 GMT+0100 (Mitteleuropäische Normalzeit)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.4.4|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>3worklistodatav4|
|**Service Type**<br>Local Cap|
|**Service URL**<br>http://localhost:4004/browse/
|**Module Name**<br>calcsheet|
|**Application Title**<br>Calculation sheet|
|**Namespace**<br>de.tammenit.sec.ui|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Code Assist Libraries**<br>False|
|**Add Eslint configuration**<br>False|
|**Enable Telemetry**<br>True|

## calcsheet

Calculation sheet result app. After the calculation has been configured in the fiori elements calcsheet_fe app this SAPUI5 freestyle app shows the results of the calculation.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply start your CAP project and navigate to the following location in your browser:

http://localhost:4004/calcsheet/webapp/index.html

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


