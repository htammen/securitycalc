sap.ui.define(["sap/fe/core/AppComponent"], function(AppComponent) {
    'use strict';

    return AppComponent.extend("de.tammenit.seccalc.ui.dayshifts.dayshifts.Component", {
        metadata: {
            manifest: "json"
        }
    });
});
