## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Tue Dec 07 2021 18:17:32 GMT+0100 (Mitteleuropäische Normalzeit)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.4.3|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>List Report Object Page V4|
|**Service Type**<br>Local Cap|
|**Service URL**<br>/browse/
|**Module Name**<br>dayshifts|
|**Application Title**<br>Day Shifts|
|**Namespace**<br>de.tammenit.seccalc.ui.dayshifts|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Code Assist Libraries**<br>False|
|**Add Eslint configuration**<br>False|
|**Enable Telemetry**<br>True|
|**Main Entity**<br>DayShifts|
|**Navigation Entity**<br>None|

## dayshifts

manage the day shifts customizing table

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply start your CAP project and navigate to the following location in your browser:

http://localhost:4004/dayshifts/webapp/index.html

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


