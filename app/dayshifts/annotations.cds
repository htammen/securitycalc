using de.tammenit.securitycalc.srv.SeccalcService as srv from '../../srv/seccalc-service';

annotate srv.DayShifts with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Value : shortTxt,
        },
        {
            $Type : 'UI.DataField',
            Value : startTime,
        },
        {
            $Type : 'UI.DataField',
            Value : endTime,
        },
        {
            $Type : 'UI.DataField',
            Value : dayShiftType_ID,
        },
        {
            $Type : 'UI.DataField',
            Value : createdAt,
        },
        {
            $Type : 'UI.DataField',
            Value : createdBy,
        },
    ],
	UI: {
		HeaderInfo: {
			TypeName: 'Schichtdaten',
			TypeNamePlural: 'Schichtdaten',
			Title : {
          $Type : 'UI.DataField',
          Value : shortTxt
      },
			Description : {
				$Type: 'UI.DataField',
				Value: descr
			},
		},
    SelectionFields: [shortTxt, startTime, endTime, dayShiftType_ID],
    Facets: [
      {$Type: 'UI.ReferenceFacet', Label: '{i18n>FieldGroup.DayShift}', Target: '@UI.FieldGroup#Main'}
    ],
    FieldGroup#Main: {
      Data: [
        {Value: startTime},
        {Value: endTime},
        {Value: dayShiftType_ID}
      ]
    }
	},    
) {
  ID @(UI.Hidden);
  dayShiftType_ID @(
    Common: {
      Label : 'Schichttyp',
      FieldControl: #Mandatory,
      Text: dayShiftType_Text,
      TextArrangement: #TextFirst,
      ValueListWithFixedValues: true,
      ValueList: {
        Label                 : '{i18n>dayshifttype}',
        $Type                 : 'Common.ValueListType',
        CollectionPath        : 'DayShiftTypeValues',
        Parameters            : [
          {
            $Type             : 'Common.ValueListParameterInOut',
            LocalDataProperty : 'dayShiftType_ID',
            ValueListProperty : 'value'
          },{
            $Type : 'Common.ValueListParameterOut',
            ValueListProperty : 'name',
            LocalDataProperty : dayShiftType_Text
          }
        ]
      }
    }
  )
};
