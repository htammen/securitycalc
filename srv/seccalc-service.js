const cds = require("@sap/cds");
const Holidays = require('date-holidays');
const sugarDate = require('sugar-date');
const containedPeriodicValues = require('contained-periodic-values');
const assert = require('assert');

class SecCalcService extends cds.ApplicationService {

  async init() {
    this.hd = new Holidays("DE", "NI");
    this.db = await cds.connect.to('db'); // connect to database service

    const SecurityEmployeeTypes = this.entities.SecurityEmployeeTypes;
    this.before("NEW", SecurityEmployeeTypes, this.onSecurityEmployeeNewHandler);

    const { CalcSheet } = this.entities;
    this.on('startCalc', CalcSheet, this.onStartCalc)

    await super.init();
  }

  onSecurityEmployeeNewHandler(req) {
    req.data.validFrom = new Date();
    req.data.validTo = new Date('2040-12-14T15:22:40.301Z');
  }

  /**
   * start the calculation of the CalcSheet
   * @param {*} req 
   */
  async onStartCalc(req) {
    const { params: [id] } = req;
    console.log(id);

    // const q1 = await SELECT.one.from( `CalcSheet`, c => {c`.*`, c.resources (r => {r`.*`}) }).where `ID = ${id.ID}`;
    // read the entire calcSheet including all referenced objects. As a result we have an object tree with all the information we 
    // need to calculate the costs.
    const calcSheet = await SELECT.one.from(`CalcSheet`)
      .columns(calcSheet => {
        calcSheet`.*`,
          calcSheet.resources(
            resources => {
              resources`.*`,
                resources.employeeType(
                  employeeType => { employeeType`.*` }
                )
            }
          ),
          calcSheet.nightShiftWeekday(
            nightShiftWeekday => { nightShiftWeekday`.*` }
          ),
          calcSheet.dayShiftWeekday(
            dayShiftWeekday => { dayShiftWeekday`.*` }
          ),
          calcSheet.nightShiftWeekend(
            nightShiftWeekend => { nightShiftWeekend`.*` }
          ),
          calcSheet.dayShiftWeekend(
            dayShiftWeekend => { dayShiftWeekend`.*` }
          ),
          calcSheet.nightShiftBankholiday(
            nightShiftBankholiday => { nightShiftBankholiday`.*` }
          ),
          calcSheet.dayShiftBankholiday(
            dayShiftBankholiday => { dayShiftBankholiday`.*` }
          )
      })
      .where`ID = ${id.ID}`;
    // const {CalcSheet} = this.entities;
    // const calcSheets = await this.db.read(CalcSheet).where({ID: id.ID});

    return this.calcCalcSheet(calcSheet);
  }

  /**
   * Calculates the costs for the entire Calculation Sheet
   * @param {*} calcSheet 
   */
  calcCalcSheet(calcSheet) {
    var result = { val: 0.0, str: `The result for ${calcSheet.name} is` }

    let startTime = new Date(calcSheet.startTime)
    let endTime = new Date(calcSheet.endTime)
    let publicHolidays = this.calcHolidays(startTime, endTime)
    let oDayCount = this.calcDayCount(startTime, endTime, publicHolidays)

    calcSheet.resources.forEach(resource => {
      result.val += this.calcSingleEmployee(resource, calcSheet, publicHolidays)
    });

    return result;
  }

  /**
   * Retrives the holidays for the period of the CalcSheet
   * @param {Date} startTime 
   * @param {Date} endTime 
   */
  calcHolidays(startTime, endTime) {
    let startYear = startTime.getFullYear()
    let endYear = endTime.getFullYear()
    let publicHolidays = []
    for (let year = startYear; year <= endYear; year++) {
      let holidays = this.hd.getHolidays(year, "de");

      publicHolidays = publicHolidays.concat(holidays.filter(holiday => { return holiday.type === "public" })
        .map(entry => { return { date: entry.date, name: entry.name } }))
    }

    publicHolidays = publicHolidays.filter(holiday => {
      let date = new Date(holiday.date);
      return !(date > endTime || date < startTime)
    })

    // console.log(publicHolidays)
    return publicHolidays
  }

  /**
   * calculates the number of weekdays, sundays, saturdays and holidays in a time period. 
   * It substracts public holidays from the calculated day counts.
   * @param {Date} startTime 
   * @param {Date} endTime 
   * @param {any[]} publicHolidays 
   * @returns {{start:Date, end:Date,total: Number, sundays: Number, saturdays: Number, weekdays: Number, holidays: Number}}
   */
  calcDayCount(startTime, endTime, publicHolidays) {
    let sugarStart = sugarDate.Date(startTime),
      sugarEnd = sugarDate.Date(endTime)

    sugarEnd.addDays(1)

    let days = 0
    let sundays = 0
    let saturdays = 0
    let weekdays = days - sundays - saturdays
    let publicHolidayCount = publicHolidays.length

    if (sugarStart.isBefore(sugarEnd.raw).raw) {
      let startDay = sugarStart.raw.getDay()
      days = sugarDate.Date.range(sugarStart.raw, sugarEnd.raw).days()
      sundays = containedPeriodicValues(startDay, days + startDay, 0, 7)
      saturdays = containedPeriodicValues(startDay, days + startDay, 6, 7)
      weekdays = days - sundays - saturdays

      publicHolidays.forEach(holiday => {
        // console.log(new Date(holiday.date))
        let sugarHoliday = sugarDate.Date(new Date(holiday.date))
        // console.log(sugarHoliday.isWeekday())
        if (sugarHoliday.isWeekday().raw) {
          weekdays = weekdays - 1
        } else if (sugarHoliday.isSaturday().raw) {
          saturdays = saturdays - 1
        } else {
          sundays = sundays - 1
        }
      })
    }

    let daysResult = { start: sugarDate.Date.short(sugarStart), end: sugarDate.Date.short(sugarEnd), total: days, sundays: sundays, saturdays: saturdays, weekdays: weekdays, holidays: publicHolidayCount }
    // console.log(daysResult)
    return daysResult
  }

  /**
   * Calculates the costs for a single employee.
   * @param {CalcSheet.Resources} employee 
   * @param {CalcSheet} calcSheet 
   * @param {any[]} publicHolidays 
   * @param {*} oDayCount 
   * @returns
   */
  calcSingleEmployee(employee, calcSheet, publicHolidays) {
    let start = new Date(Math.max(new Date(employee.startTime), new Date(calcSheet.startTime)))
    let end = new Date(Math.min(new Date(employee.endTime), new Date(calcSheet.endTime)))
    let oDayCount = this.calcDayCount(start, end, publicHolidays)

    let weekdayDayShiftsCosts = this.calcTimePeriodCosts(employee, calcSheet, oDayCount.weekdays, "weekday", "dayShiftWeekday", "dayRate")
    let weekdayNightShiftsCosts = this.calcTimePeriodCosts(employee, calcSheet, oDayCount.weekdays, "weekday", "nightShiftWeekday", "nightRate")
    let weekendDayShiftsCosts = this.calcTimePeriodCosts(employee, calcSheet, oDayCount.saturdays + oDayCount.sundays, "weekend", "dayShiftWeekend", "dayRateWeekend")
    let weekendNightShiftsCosts = this.calcTimePeriodCosts(employee, calcSheet, oDayCount.saturdays + oDayCount.sundays, "weekend", "nightShiftWeekend", "nightRateWeekend")
    let bankholidayDayShiftsCosts = this.calcTimePeriodCosts(employee, calcSheet, oDayCount.holidays, "bankholiday", "dayShiftBankholiday", "dayRateBankholiday")
    let bankholidayNightShiftsCosts = this.calcTimePeriodCosts(employee, calcSheet, oDayCount.holidays, "bankholiday", "nightShiftBankholiday", "nightRateBankholiday")

    return { weekdayDayShiftsCosts: weekdayDayShiftsCosts, 
      weekdayNightShiftsCosts: weekdayNightShiftsCosts, 
      weekendDayShiftsCosts: weekendDayShiftsCosts, 
      weekendNightShiftsCosts: weekendNightShiftsCosts,
      bankholidayDayShiftsCosts: bankholidayDayShiftsCosts,
      bankholidayNightShiftsCosts: bankholidayNightShiftsCosts
    };
  }

  calcTimePeriodCosts(employee, calcSheet, nDays, sTimePeriod, sShift, sRate, ) {
    try {
      function round(num) {
        var m = Number((Math.abs(num) * 100).toPrecision(15));
        return Math.round(m) / 100 * Math.sign(num);
      }      

      const employeeStartTime = sugarDate.Date(employee[sTimePeriod + "StartTime"])
      let employeeEndTime = sugarDate.Date(employee[sTimePeriod + "EndTime"])
      if (employeeEndTime.isBefore(employeeStartTime.raw).raw) {
        employeeEndTime = employeeEndTime.addDays(1)
      }
      let employeeTimeRange = sugarDate.Date.range(employeeStartTime.raw, employeeEndTime.raw)

      const cfgStart = sugarDate.Date(calcSheet[sShift].startTime)
      let cfgEnd = sugarDate.Date(calcSheet[sShift].endTime)
      if (cfgEnd.isBefore(cfgStart.raw).raw) {
        cfgEnd = cfgEnd.addDays(1)
      }
      const cfgTimeRange = sugarDate.Date.range(cfgStart.raw, cfgEnd.raw)

      // if end of work > start of shift we need the hours in between cause they are not calculated by the
      // intersect method.
      const hoursBetweenEndOfWorkAndStartOfShift = employeeEndTime.raw.getDate() > cfgStart.raw.getDate()
        ? employeeEndTime.raw.getHours() - cfgStart.raw.getHours()
        : 0.0
      const overlappingHoursBefore = cfgEnd.raw.getDate() > employeeStartTime.raw.getDate()
        ? cfgEnd.raw.getHours() - employeeStartTime.raw.getHours()
        : 0.0

      const intersectTimeRange = cfgTimeRange.intersect(employeeTimeRange)
      const hours = round(((!intersectTimeRange || !intersectTimeRange.start || !intersectTimeRange.end) ? 0.0 : 
        (intersectTimeRange.milliseconds() + (sugarDate.Date(intersectTimeRange.end).format('{HH}{mm}{ss}') === '235959' ? 1 : 0 )) / 3600000) +
        (hoursBetweenEndOfWorkAndStartOfShift > 0 ? hoursBetweenEndOfWorkAndStartOfShift : 0.0) +
        (overlappingHoursBefore > 0 ? overlappingHoursBefore : 0.0))


      const retValue = employee.employeeType[sRate] * hours * nDays
      return { value: retValue, hours: hours, days: nDays }
    } catch (error) {
      console.error(error)
      return {value: 0.0, hours: 0, days: 0}
    }
    // const employeeShiftStartTime = sugarDate.Date(employee[sTimePeriod + "StartTime"])
    // let employeeShiftEndTime = sugarDate.Date(employee[sTimePeriod + "EndTime"])
    // if(employeeShiftEndTime.isBefore(employeeShiftStartTime.raw).raw) {
    //   // employeeWeekdayEndTime = employeeWeekdayEndTime.addDays(1)
    // }
    
    // const cfgShiftStart = sugarDate.Date(calcSheet[sShift].startTime)
    // let cfgShiftEnd = sugarDate.Date(calcSheet[sShift].endTime)

    // const overlappingMinutes = this.calcOverlappingTime(employeeShiftStartTime, employeeShiftEndTime, cfgShiftStart, cfgShiftEnd)
    // return employee.employeeType[sRate] * overlappingMinutes/60 * nDays

  }

  /**
   * @deprecated
   * @param {*} employeeShiftStartTime 
   * @param {*} employeeShiftEndTime 
   * @param {*} cfgShiftStartTime 
   * @param {*} cfgShiftEndTime 
   */
  calcOverlappingTime(employeeShiftStartTime, employeeShiftEndTime, cfgShiftStartTime, cfgShiftEndTime) {
    let startTime, endTime,
        normalizedEmployeeShiftEndTime,
        normalizedCfgShiftEndTime,
        overlappingMinutes = 0.0

    if(employeeShiftEndTime.raw < employeeShiftStartTime.raw) {
      normalizedEmployeeShiftEndTime = employeeShiftEndTime.addDays(1)
    } else {
      normalizedEmployeeShiftEndTime = employeeShiftEndTime
    }
    if(cfgShiftEndTime.raw < cfgShiftStartTime.raw) {
      normalizedCfgShiftEndTime = cfgShiftEndTime.addDays(1)
    } else {
      normalizedCfgShiftEndTime = cfgShiftEndTime
    }

    // overlapping time of employee worktime with cfgShift time at beginning of employeeShift and in between
    // start and end of Shift
    if(employeeShiftStartTime.raw < normalizedCfgShiftEndTime.raw && employeeShiftStartTime.raw > cfgShiftStartTime.raw) {
      startTime = employeeShiftStartTime.raw
      endTime = Math.min(normalizedCfgShiftEndTime.raw, normalizedEmployeeShiftEndTime.raw)
      let timeRange = sugarDate.Date.range(startTime, endTime)
      overlappingMinutes += timeRange.minutes()
    }
    // overlapping time of employee worktime between cfgShift start and cfgShiftEnd
    if(employeeShiftEndTime.raw > cfgShiftStartTime.raw && employeeShiftEndTime.raw < cfgShiftEndTime.raw) {
      startTime = cfgShiftStartTime.raw
      endTime = Math.max(employeeShiftEndTime.raw, cfgShiftStartTime.raw)
      let timeRange = sugarDate.Date.range(startTime, endTime)
      overlappingMinutes += timeRange.minutes()
    }
    return overlappingMinutes
  }
}
module.exports = SecCalcService;

