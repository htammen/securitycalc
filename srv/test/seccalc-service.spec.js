import SecCalcService from "../seccalc-service.js"

describe('SecCalcService calcHolidays Suite', () => {
  let srv;

  beforeAll(() => {
    srv = new SecCalcService()
    srv.init()
  });

  it("should return 10 holidays for 2022", () => {
    let holidays = srv.calcHolidays(new Date('01 Jan 2022 00:00:00 GMT'), new Date('31 Dec 2022 23:59:59 GMT'))
    expect(holidays.length).toBe(10)
  })

  it("should return 26/05/2022 for 'Christi Himmelfahrt'", () => {
    let holidays = srv.calcHolidays(new Date('01 Jan 2022 00:00:00 GMT'), new Date('31 Dec 2022 23:59:59 GMT'))
    let myHoliday = holidays.filter(holiday => {
      return holiday.name === 'Christi Himmelfahrt'
    })[0]
    expect(holidays.length).toBe(10)
    expect(myHoliday.date).toBe('2022-05-26 00:00:00')
  })

  it("should return 31/10/2022 for 'Reformationstag'", () => {
    let holidays = srv.calcHolidays(new Date('01 Jun 2022 00:00:00 GMT'), new Date('01 Nov 2022 23:59:59 GMT'))
    let myHoliday = holidays.filter(holiday => {
      return holiday.name === 'Reformationstag'
    })[0]
    expect(holidays.length).toBe(3)
    expect(myHoliday.date).toBe('2022-10-31 00:00:00')
  })

  it("should not find 'Reformationstag' between 01/01/2022 and 30/10/2022", () => {
    let holidays = srv.calcHolidays(new Date('01 Jan 2022 00:00:00 GMT'), new Date('30 Oct 2022 23:59:59 GMT'))
    let myHoliday = holidays.filter(holiday => {
      return holiday.name === 'Reformationstag'
    })
    expect(myHoliday.length).toBe(0)
  })

  it("TODO: should find 'Reformationstag between 31/10/2022 08:00:00 and 31/10/2022 12:00:00'", () => {
    let holidays = srv.calcHolidays(new Date('31 Oct 2022 08:00:00 GMT'), new Date('31 Oct 2022 12:00:00 GMT'))
    let myHoliday = holidays.filter(holiday => {
      return holiday.name === 'Reformationstag'
    })
    // TODO: This use case is not handled correctly yet
    expect(myHoliday.length).toBe(0)
  })
});

describe('SecCalcService calcWeekdays Suite', () => {
  let srv;

  beforeAll(() => {
    srv = new SecCalcService()
    srv.init()
  });

  it("should return 10 weekdays, 2 saturdays, 2 sundays and 0 holidays between 10/01/2022 and 24/01/2022", () => {
    let startDate = new Date('01-10-2022'),
        endDate = new Date('01-24-2022')
    let publicHolidays = srv.calcHolidays(startDate, endDate)
    let weekdayCount = srv.calcDayCount(startDate, endDate, publicHolidays)
    expect(weekdayCount.weekdays).toBe(11)
    expect(weekdayCount.saturdays).toBe(2)
    expect(weekdayCount.sundays).toBe(2)
    expect(weekdayCount.holidays).toBe(0)
  })
  it("should return 33 weekdays, 7 saturdays, 7 sundays and 4 holydays between 10/04/2022 and 30/05/2022", () => {
    let startDate = new Date('04-10-2022'),
        endDate = new Date('05-31-2022')
    let publicHolidays = srv.calcHolidays(startDate, endDate)
    let weekdayCount = srv.calcDayCount(startDate, endDate, publicHolidays)
    expect(weekdayCount.weekdays).toBe(34)
    expect(weekdayCount.saturdays).toBe(7)
    expect(weekdayCount.sundays).toBe(7)
    expect(weekdayCount.holidays).toBe(4)
  })
  it("should return 253 weekdays, 52 saturdays, 50 sundays and 10 holydays between 01/01/2022 and 31/12/2022", () => {
    let startDate = new Date('01-01-2022'),
        endDate = new Date('12-31-2022')
    let publicHolidays = srv.calcHolidays(startDate, endDate)
    let weekdayCount = srv.calcDayCount(startDate, endDate, publicHolidays)
    expect(weekdayCount.weekdays).toBe(253)
    expect(weekdayCount.saturdays).toBe(52)
    expect(weekdayCount.sundays).toBe(50)
    expect(weekdayCount.holidays).toBe(10)
  })
  it("should return 254 weekdays, 52 saturdays, 49 sundays and 10 holydays between 21/01/2022 and 20/01/2023", () => {
    let startDate = new Date('01-21-2022'),
        endDate = new Date('01-20-2023')
    let publicHolidays = srv.calcHolidays(startDate, endDate)
    let weekdayCount = srv.calcDayCount(startDate, endDate, publicHolidays)
    expect(weekdayCount.weekdays).toBe(254)
    expect(weekdayCount.saturdays).toBe(52)
    expect(weekdayCount.sundays).toBe(49)
    expect(weekdayCount.holidays).toBe(10)
  })
  it("should return 252 weekdays, 52 saturdays, 52 sundays and 10 holydays between 01/01/2024 and 31/12/2024", () => {
    let startDate = new Date('01-01-2024'),
        endDate = new Date('12-31-2024')
    let publicHolidays = srv.calcHolidays(startDate, endDate)
    let weekdayCount = srv.calcDayCount(startDate, endDate, publicHolidays)
    expect(weekdayCount.weekdays).toBe(252)
    expect(weekdayCount.saturdays).toBe(52)
    expect(weekdayCount.sundays).toBe(52)
    expect(weekdayCount.holidays).toBe(10)
  })
  it("should return 756 weekdays, 157 saturdays, 153 sundays and 30 holydays between 12/01/2022 and 11/01/2025", () => {
    let startDate = new Date('01-12-2022'),
        endDate = new Date('01-11-2025')
    let publicHolidays = srv.calcHolidays(startDate, endDate)
    let weekdayCount = srv.calcDayCount(startDate, endDate, publicHolidays)
    expect(weekdayCount.weekdays).toBe(756)
    expect(weekdayCount.saturdays).toBe(157)
    expect(weekdayCount.sundays).toBe(153)
    expect(weekdayCount.holidays).toBe(30)
  })
})
