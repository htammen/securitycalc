import SecCalcService from "../seccalc-service.js"

describe('SecCalcService calcSingleEmployee Suite', () => {
  let srv;

  beforeAll(() => {
    srv = new SecCalcService()
    srv.init()
  });

  it("should only calculate weekday dayshift values, nightshift = 0. Empl. only working dayshift", () => {
    let employee = {
      startTime: '2022-01-26T07:00:00Z',
      endTime: '2022-03-26T07:00:00Z',
      weekdayStartTime: '08:00:00',
      weekdayEndTime: '10:30:00',
      weekendStartTime: '00:00:00',
      weekendEndTime: '23:59:59',
      employeeType: {
        dayRate: 15.60,
        nightRate: 18.25
      }
    }
    // define calcsheet parameters
    let calcSheet = {
      startTime: '2022-01-20T07:00:00Z',
      endTime: '2022-04-29T07:00:00Z',
      dayShiftWeekday: {
        startTime: '07:00:00',
        endTime: '18:00:00',
      },
      nightShiftWeekday: {
        startTime: '18:00:00',
        endTime: '08:00:00',
      }
    }
    let start = new Date(Math.max(new Date(employee.startTime), new Date(calcSheet.startTime)))
    let end = new Date(Math.min(new Date(employee.endTime), new Date(calcSheet.endTime)))
    const publicHolidays = srv.calcHolidays(start, end)
    let singleEmployeeCosts = srv.calcSingleEmployee(employee, calcSheet, publicHolidays)
    // 43 weekdays * 2.5 hours * 15.6,- = 1677,-
    expect(singleEmployeeCosts.weekdayDayShiftsCosts.value).toBe(1677)
    // employee does not work at night
    expect(singleEmployeeCosts.weekdayNightShiftsCosts.value).toBe(0)
  })

  it("should calculate weekday dayshift values in a time period with 3 weekday hollidays", () => {
    let employee = {
      startTime: '2022-03-01T07:00:00Z',
      endTime: '2022-08-10T07:00:00Z',
      weekdayStartTime: '12:00:00',
      weekdayEndTime: '15:30:00',
      employeeType: {
        dayRate: 15,
        nightRate: 20
      }
    }
    let calcSheet = {
      startTime: '2022-04-01T07:00:00Z',
      endTime: '2022-05-10T07:00:00Z',
      dayShiftWeekday: {
        startTime: '07:00:00',
        endTime: '18:00:00',
      },
      nightShiftWeekday: {
        startTime: '18:00:00',
        endTime: '08:00:00',
      }
    }
    let start = new Date(Math.max(new Date(employee.startTime), new Date(calcSheet.startTime)))
    let end = new Date(Math.min(new Date(employee.endTime), new Date(calcSheet.endTime)))
    const publicHolidays = srv.calcHolidays(start, end)
    let singleEmployeeCosts = srv.calcSingleEmployee(employee, calcSheet, publicHolidays)
    // 26 weekdays * 3.5 hours * 15,- = 1.365,-
    expect(singleEmployeeCosts.weekdayDayShiftsCosts.value).toBe(1365)
    // employee does not work at night
    expect(singleEmployeeCosts.weekdayNightShiftsCosts.value).toBe(0)
  })

  it("should calculate result of 0.0 cause empl. times are not within calcsheet times", () => {
    let employee = {
      startTime: '2022-01-26T07:00:00Z',
      endTime: '2022-03-26T07:00:00Z',
      weekdayStartTime: '15:00:00',
      weekdayEndTime: '23:59:59',
      weekendStartTime: '00:00:00',
      weekendEndTime: '23:59:59',
      employeeType: {
        dayRate: 20,
        nightRate: 25
      }
    }
    let calcSheet = {
      startTime: '2022-04-01T07:00:00Z',
      endTime: '2022-06-15T07:00:00Z',
      dayShiftWeekday: {
        startTime: '07:00:00',
        endTime: '18:00:00',
      },
      nightShiftWeekday: {
        startTime: '18:00:00',
        endTime: '08:00:00',
      }
    }
    let start = new Date(Math.max(new Date(employee.startTime), new Date(calcSheet.startTime)))
    let end = new Date(Math.min(new Date(employee.endTime), new Date(calcSheet.endTime)))
    const publicHolidays = srv.calcHolidays(start, end)
    let singleEmployeeCosts = srv.calcSingleEmployee(employee, calcSheet, publicHolidays)
    // employee timeframe not within calcsheet timeframe --> 0.0
    expect(singleEmployeeCosts.weekdayDayShiftsCosts.value).toBe(0)
    // employee timeframe not within calcsheet timeframe --> 0.0
    expect(singleEmployeeCosts.weekdayNightShiftsCosts.value).toBe(0)
  })
  
  /**
   * This is a medium complex scenario with mostly nightshift work. The shift are as follows
   * dayshfit: 7:00 - 18:00
   * nightshift: 18:00 - 7:00
   * Employee worktime:
   * 17:00 - 8:00
   * This scenario does not consider weekend work and holiday work
   */
  it("should calculate a scenario for for mostly nightshift work with small dayshift times", () => {
    let employee = {
      startTime: '2022-01-01T07:00:00Z',
      endTime: '2022-12-31T07:00:00Z',
      weekdayStartTime: '17:00:00',
      weekdayEndTime: '08:00:00',
      weekendStartTime: '00:00:00',
      weekendEndTime: '23:59:59',
      employeeType: {
        dayRate: 20,
        nightRate: 25
      }
    }
    /* time period has 4 weekday holidays and 1 sunday holiday */
    let calcSheet = {
      startTime: '2022-04-01T07:00:00Z',
      endTime: '2022-06-15T07:00:00Z',
      dayShiftWeekday: {
        startTime: '07:00:00',
        endTime: '18:00:00',
      },
      nightShiftWeekday: {
        startTime: '18:00:00',
        endTime: '07:00:00'
      }
    }
    let start = new Date(Math.max(new Date(employee.startTime), new Date(calcSheet.startTime)))
    let end = new Date(Math.min(new Date(employee.endTime), new Date(calcSheet.endTime)))
    const publicHolidays = srv.calcHolidays(start, end)
    let singleEmployeeCosts = srv.calcSingleEmployee(employee, calcSheet, publicHolidays)
    // 50 weekdays * 2 hours * 20,- = 2.000,-
    expect(singleEmployeeCosts.weekdayDayShiftsCosts.value).toBe(2000)
    // 50 weekdays * 13 hours * 25,- = 16.250,-
    expect(singleEmployeeCosts.weekdayNightShiftsCosts.value).toBe(16250)
  })

  /**
   * TBD
   * This is a medium complex scenario with mostly nightshift work. The shift are as follows
   * dayshfit: 7:00 - 18:00
   * nightshift: 18:00 - 7:00
   * Employee worktime:
   * 17:00 - 8:00
   * This scenario does not consider weekend work and holiday work
   */
  it("should calculate a scenario for for mostly nightshift work with small dayshift times. It also considers weekend work and holiday work", () => {
    let employee = {
      startTime: '2022-01-01T07:00:00Z',
      endTime: '2022-12-31T07:00:00Z',
      weekdayStartTime: '17:00:00',
      weekdayEndTime: '08:00:00',
      weekendStartTime: '00:00:00',
      weekendEndTime: '23:59:59',
      bankholidayStartTime: '00:00:00',
      bankholidayEndTime: '23:59:59',
      employeeType: {
        dayRate: 20,
        nightRate: 25,
        dayRateWeekend: 30,
        nightRateWeekend: 35,
        dayRateBankholiday: 40,
        nightRateBankholiday: 45
      }
    }
    /* time period has 4 weekday holidays and 1 sunday holiday */
    let calcSheet = {
      startTime: '2022-04-01T07:00:00Z',
      endTime: '2022-06-15T07:00:00Z',
      dayShiftWeekday: {
        startTime: '07:00:00',
        endTime: '18:00:00',
      },
      nightShiftWeekday: {
        startTime: '18:00:00',
        endTime: '07:00:00'
      },
      dayShiftWeekend: {
        startTime: '09:00:00',
        endTime: '17:00:00',
      },
      nightShiftWeekend: {
        startTime: '17:00:00',
        endTime: '09:00:00'
      },
      dayShiftBankholiday: {
        startTime: '09:00:00',
        endTime: '17:00:00',
      },
      nightShiftBankholiday: {
        startTime: '17:00:00',
        endTime: '09:00:00'
      }
    }
    let start = new Date(Math.max(new Date(employee.startTime), new Date(calcSheet.startTime)))
    let end = new Date(Math.min(new Date(employee.endTime), new Date(calcSheet.endTime)))
    const publicHolidays = srv.calcHolidays(start, end)
    let singleEmployeeCosts = srv.calcSingleEmployee(employee, calcSheet, publicHolidays)
    // 50 weekdays * 2 hours * 20,- = 2.000,-
    expect(singleEmployeeCosts.weekdayDayShiftsCosts.value).toBe(2000)
    // 50 weekdays * 13 hours * 25,- = 16.250,-
    expect(singleEmployeeCosts.weekdayNightShiftsCosts.value).toBe(16250)
    // 21 weekend days * 8 hours * 30,- = 5.040,-
    expect(singleEmployeeCosts.weekendDayShiftsCosts.value).toBe(5040)
    // 21 weekend days * 16 hours * 35,- = 11.760,-
    expect(singleEmployeeCosts.weekendNightShiftsCosts.value).toBe(11760)
    // 5 bankholidays * 8 hours * 40,- = 1.600,-
    expect(singleEmployeeCosts.bankholidayDayShiftsCosts.value).toBe(1600)
    // 5 bankholidays * 16 hours * 45,- = 3.600,-
    expect(singleEmployeeCosts.bankholidayNightShiftsCosts.value).toBe(3600)
  })
})