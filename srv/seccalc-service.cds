namespace de.tammenit.securitycalc.srv;
using de.tammenit.securitycalc as db from '../db/schema';
using de.tammenit.securitycalc.cust as cust from '../db/schema_cust';

service SeccalcService @(path:'/browse') {

  type CalcResult: {val: Decimal; str: String};
  entity CalcSheet as projection on db.CalcSheet excluding { createdBy, modifiedBy } actions {
    function startCalc() returns CalcResult; 
  };
  annotate CalcSheet with @odata.draft.enabled ;

  entity SecurityEmployeeTypes as projection on db.SecurityEmployeeTypes excluding {createdBy, modifiedBy};
  annotate SecurityEmployeeTypes with @odata.draft.enabled;
  entity DayShifts             as select * from db.DayShifts;
  annotate DayShifts with @odata.draft.enabled;

  entity DayTypes              as select * from cust.DayTypes;
  annotate DayTypes with @odata.draft.enabled;

  entity DayShiftTypeValues as select * from cust.DayShiftTypeValues;
  annotate DayShiftTypeValues with {
    value @UI.Hidden;
  };
  
}
