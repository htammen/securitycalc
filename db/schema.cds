namespace de.tammenit.securitycalc;

using {
  Currency,
  managed,
  sap,
  temporal,
  cuid
} from '@sap/cds/common';

using {de.tammenit.securitycalc.cust.DayTypes, 
        de.tammenit.securitycalc.cust.DayShiftTypeValues} from './schema_cust';


entity CalcSheet : managed {
  key ID                         : UUID                 @(
        Core.Computed : true,
        title         : 'ID'
      );
      name                       : localized String(120)@(title : '{i18n>Calcsheet.name}');
      descr                      : localized LargeString;
      startTime                  : DateTime;
      endTime                    : DateTime;
      resources                  : Composition of many CalcSheet.Resources;
      dayShiftWeekday            : Association to DayShifts;
      dayShiftWeekday_Text       : String;
      nightShiftWeekday          : Association to DayShifts;
      nightShiftWeekday_Text     : String;
      dayShiftWeekend            : Association to DayShifts;
      dayShiftWeekend_Text       : String;
      nightShiftWeekend          : Association to DayShifts;
      nightShiftWeekend_Text     : String;
      dayShiftBankholiday        : Association to DayShifts;
      dayShiftBankholiday_Text   : String;
      nightShiftBankholiday      : Association to DayShifts;
      nightShiftBankholiday_Text : String;
}

// aspect CalcSheet.Resources : managed {
//   /**
//    * unique ID
//    */
//   key ID               : UUID;
//   /**
//    * name of the resource
//    */
//   name                 : localized String(80);
//   /**
//    * defines how many resources of this type are needed to secur
//    * the object
//    */
//   amount               : Integer;
//   /**
//    * Refrence to a SecurityEmployeeTypes entry
//    */
//   employeeType         : Association to SecurityEmployeeTypes;
//   employeeType_Text    : String;
//   /**
//    * starttime of the employee in general
//    */
//   startTime            : DateTime;
//   /**
//    * endtime of the employee in general
//    */
//   endTime              : DateTime;
//   /**
//    * Starttimes and endtimes for the several days.
//    */
//   startEndTimes        : Composition of CalcSheet.StartEndTimes;
// }

/**
A CalcSheet.Resource is an abstract employee description. It's not a person with name, birthday, ... but a description of an
employee that is used in the CalcSheet. That said a Resource can e.g. have a start time of 00:00 and an end time of 23:59 
 */
aspect CalcSheet.Resources: managed {
  /** unique ID */    
  key ID: UUID;
  /** name of the resource */
  name: localized String(80);
  /** defines how many resources of this type are needed to secur the object */
  amount: Integer;
  /** Refrence to a SecurityEmployeeTypes entry */
  employeeType: Association to SecurityEmployeeTypes;
  employeeType_Text: String;
  startTime: DateTime;
  endTime: DateTime;
  /** weekday starttime of this resource */
  weekdayStartTime: Time;
  /** weekday endtime of this resource */
  weekdayEndTime: Time;
  /** weekend starttime of this resource */
  weekendStartTime: Time;
  /** weekend endtime of this resource */
  weekendEndTime: Time;
  /** bank holiday starttime of this resource */
  bankholidayStartTime: Time;
  /** bank holiday endtime of this resource */
  bankholidayEndTime: Time;
}    

/*    
 * StartEndTimes is an entity that stores the start and end times for a special day.
 * The day can be the weekday (monday, tuesday, ...) but also a special day like 
 * bank holiday.
 * The types (dayType) are interpreted in the business logic. E.g. if a weekday is
 * a bank holiday this day is tread as bank holiday, not as weekday
 */
aspect CalcSheet.StartEndTimes : managed {
  starttime : Time;
  endTime   : Time;
  key day_ID    : Integer;
  day_Text  : String;
  dayType: Association to DayShiftTypeValues on dayType.value = day_ID @(title: 'Type');
}

type DayShiftType: Integer @assert.range enum {
  weekday = 1;
  weekend = 2;
  bankholiday = 3;
}

@cds.odata.valuelist
entity DayShifts: managed {
  key ID: UUID @(Core.Computed : true, title: 'ID');
  shortTxt: localized String(60) @(title: 'Short text');
  descr: localized LargeString @(title: 'Description');
  startTime: Time @(title: 'Start time');
  endTime: Time @(title: 'End time');
  dayShiftType_ID: Integer;
  dayShiftType_Text: String;
  dayShiftType: Association to DayShiftTypeValues on dayShiftType.value = dayShiftType_ID @(title: 'Type');
}

@cds.odata.valuelist
entity SecurityEmployeeTypes: managed, temporal {
  /** unique ID */
  key ID: UUID @(Core.Computed : true, title: 'ID', Common.Text: shortTxt);
  /** a short text (label) for this type. E.g. security worker, chief officer, ... */
  shortTxt: localized String(60) @(title: 'Short text');
  /** a longer description of this type */
  descr: localized String(60) @(title: 'Description');
  /** hourly rate for worktime at day hours */
  dayRate: Decimal(6,2) @(title: 'Rate at day');
  /** hourly rate for worktime at night hours */
  nightRate: Decimal(6,2) @(title: 'Rate at night');
  /** hourly rate for worktime at weekend day hours */
  dayRateWeekend: Decimal(6,2) @title : 'Day rate at weekend';
  /** hourly rate for worktime at weekend night hours */
  nightRateWeekend: Decimal(6,2) @title : 'Night rate at weekend';
  /** hourly rate for worktime at bank holiday hours */
  bankholidayRate: Decimal(6,2) @(title: 'Rate at bank holiday');
}

