namespace de.tammenit.securitycalc.cust;

using {
  Currency,
  managed,
  sap,
  temporal,
  cuid
} from '@sap/cds/common';

@cds.odata.valuelist
entity DayTypes {
  key id : Integer;
      text : localized String(30);
      dayType_ID: Integer;
      dayType_Text: String;
      type: Association to DayShiftTypeValues on type.value = dayType_ID @(title: 'Type')
}

@cds.odata.valuelist
entity DayShiftTypeValues {
  key value: Integer;
  name: String;
}
